<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * Passwords
 */

/**
 * Minimum Entropy (password strength)
 *
 * The minimum password strength that a password must meet to be
 * considered a strong-enough value. While the formula is a bit complex
 * you can use the following guidelines:
 * - 18 bits of entropy = minimum for ANY website.
 * - 25 bits of entropy = minimum for a general purpose web service used 
 *   relatively widely (e.g. Hotmail).
 * - 30 bits of entropy = minimum for a web service with business critical 
 *   applications (e.g. SAAS).
 * - 40 bits of entropy = minimum for a bank or other financial service.
 */
$config['auth.min_password_strength'] = 8;

/**
 * Password Hashing Cost
 *
 * The BCRYPT method of encryption allows you to define the "cost"
 * or number of iterations made, whenver a password hash is created.
 * This defaults to a value of 10 which is an acceptable number.
 * However, depending on the security needs of your application
 * and the power of your hardware, you might want to increase the
 * cost. This makes the hasing process takes longer.
 *
 * Valid range is between 4 - 31.
 */
$config['auth.hash_cost'] = 5;

/**
 * Activation Method
 *
 * The site supports 3 methods of activating a user:
 * - 'auto'    No extra protection, they are allowed in site after signup.
 * - 'email'   The are sent an email with an activation link/code
 * - 'manual'  Requires manual approval by a site administrator.
 */
$config['auth.activation_method'] = 'auto';


/*
 * Roles
 */

/**
 * Default Role ID
 *
 * Sets the Default role id to use when creating new users.
 */
$config['auth.default_role_id'] = 1;

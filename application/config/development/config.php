<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Site URL
 *
 * URL to your CodeIgniter root. Typically this will be your base URL,
 * WITH a trailing slash:
 *
 *     http://example.com/
 *
 * **Warning:** You must set this value!
 *
 * If it is not set, then CodeIgniter will try guess the protocol and path
 * your installation, but due to security concerns the hostname will be set
 * to $_SERVER['SERVER_ADDR'] if available, or localhost otherwise.
 * The auto-detection mechanism exists only for convenience during
 * development and MUST NOT be used in production!
 *
 * If you need to allow multiple domains, remember that this file is still
 * a PHP script and you can easily do that on your own.
 */
$config['base_url'] = 'http://xlt.dev';

/**
 * Composer auto-loading
 *
 * Enabling this setting will tell CodeIgniter to look for a Composer
 * package auto-loader script in *application/vendor/autoload.php*.
 *
 *     $config['composer_autoload'] = TRUE;
 *
 * Or if you have your *vendor/* directory located somewhere else, you
 * can opt to set a specific path as well:
 *
 *     $config['composer_autoload'] = '/path/to/vendor/autoload.php';
 *
 * **Note:** This will not disable or override the CodeIgniter-specific
 *           autoloading (*application/config/autoload.php*).
 *
 * @see See the [Composer website](http://getcomposer.org/) for more 
 *      information.
 */
$config['composer_autoload'] = TRUE;

/**
 * Error Logging Threshold
 *
 * You can enable error logging by setting a threshold over zero. The
 * threshold determines what gets logged. Threshold options are:
 *
 *     0 = Disables logging, Error logging TURNED OFF
 *     1 = Error Messages (including PHP errors)
 *     2 = Debug Messages
 *     3 = Informational Messages
 *     4 = All Messages
 *
 * You can also pass an array with threshold levels to show individual error 
 * types
 *
 *     array(2) = Debug Messages, without Error Messages
 *
 * For a live site you'll usually only enable Errors (1) to be logged, otherwise
 * your log files will fill up very fast.
 */
$config['log_threshold'] = 3;

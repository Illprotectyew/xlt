<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Auto Migrate?
 *
 * We can automatically run any outstanding migrations in the core, the 
 * application and the modules themselves if this is set to `true`.
 */
$config['auto_migrate'] = array(
//  'app', // Comment this line out to turn off auto-migrations.
);

/**
 * Profiler
 */
$config['show_profiler'] = TRUE;

/**
 * PHP Error
 *
 * If enabled, will use a custom error screen on PHP errors instead of a generic 
 * screen. Only works in the development environment, for all other environments 
 * it is ignored.
 */
$config['use_php_error'] = TRUE;

/**
 * Caching
 *
 * Sets the default types of caching used throughout the site. Possible choices 
 * are `apc`, `file`, `memcached` and `dummy`.
 *
 * If you don't wish to use any caching in your environment, set it to dummy.
 *
 * The cache types can be overriedden as class values within each controller.
 */
$config['cache_type']           = 'dummy';
$config['backup_cache_type']    = 'dummy';

/**
 * Mail: Pretend to send
 *
 * When set to `true`, this setting tells Sprint to pretend to send emails and 
 * simply return a successful send. Useful during certain stages of testing.
 */
$config['mail.pretend'] = TRUE;

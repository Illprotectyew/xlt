<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Allowed Environments
 *
 * Before any _generators are run, the current environment will be
 * tested to verify it's an allowed environment.
 */
$config['forge.allowed_environments'] = [
    'development',
    'testing',
    'travis'
];

/**
 * Themer to Use
 *
 * Define the themer to use when rendering our template files. This should 
 * include the fully namespaced classname.
 */
$config['forge.themer'] = '\Myth\Themers\ViewThemer';

/**
 * Generator Collections
 *
 * Defines the locations to look for generator and their templates. These will
 * be searched in the order listed in the array. This allows you to customize 
 * just one or two files for this project or your company styles and still have 
 * all other templates from the Sprint group.
 *
 * The 'keys' are aliases that can be used to reference the view from.
 */
$config['forge.collections'] = [
    'sprint' => MYTHPATH .'_generators/'
];

<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * The `$active_group` variable lets you choose which connection group to
 * make active. By default there is only one group (the `'default'` group).
 *
 * @var string $active_group The connection group to make active.
 */
$active_group = 'default';

/**
 * The `$query_builder` variable lets you determine whether or not to load
 * the query builder class.
 *
 * @var bool $query_builder Whether or not to load the query builder class.
 *                          Accepts `true` or `false`.
 */
$query_builder = TRUE;

/**
 * Database Connectivity Settings
 * 
 * @var array $db A database connection settings group.
 *
 * @see See the user guide for documentation on [Database Connections]
 *      (https://codeigniter.com/user_guide/database/configuration.html).
 */
$db['default'] = array(
    'dsn'      => '',
    'hostname' => 'localhost',
    'username' => 'root',
    'password' => '',
    'database' => 'catt',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => TRUE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

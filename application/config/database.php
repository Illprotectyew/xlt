<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * The `$active_group` variable lets you choose which connection group to
 * make active. By default there is only one group (the `'default'` group).
 *
 * @var string $active_group The connection group to make active.
 */
$active_group = 'default';

/**
 * The `$query_builder` variable lets you determine whether or not to load
 * the query builder class.
 *
 * @var bool $query_builder Whether or not to load the query builder class.
 *                          Accepts `true` or `false`.
 */
$query_builder = TRUE;

/**
 * Database Connectivity Settings
 * 
 * This file will contain the settings needed to access your database.
 *
 * @var array $db {
 *     A database connection settings group.
 *
 *     @type string 'dsn'      The full DSN string describe a connection to the 
 *                             database.
 *     @type string 'hostname' The hostname of your database server.
 *     @type string 'username' The username used to connect to the database.
 *     @type string 'password' The password used to connect to the database.
 *     @type string 'database' The name of the database you want to connect to.
 *     @type string 'dbdriver' The database driver. Accepts `cubrid`, `ibase`, 
 *                             `mssql`, `mysql`, `mysqli`, `oci8`, `odbc`, `pdo`, 
 *                             `postgre`, `sqlite`, `sqlite3` & `sqlsrv`. 
 *     @type string 'dbprefix' An optional prefix added to table names when using 
 *                             the Query Builder class.
 *     @type bool   'pconnect' Whether or not to use a persistent connection. 
 *                             Accepts `true` or `false`.
 *     @type bool   'db_debug' Whether or not database errors should be displayed. 
 *                             Accepts `true` or `false`.
 *     @type bool   'cache_on' Whether or not to enable database query caching. 
 *                             Accepts `true` or `false`.
 *     @type string 'cachedir' The path to the folder where query cache files 
 *                             should be stored.
 *     @type string 'char_set' The character set used for communicating with the 
 *                             database.
 *     @type string 'dbcollat' The character collation used for communicating 
 *                             with the database. **Note:** For MySQL and MySQLi 
 *                             databases, this setting is only used as a backup 
 *                             if your server is running PHP < 5.2.3 or MySQL < 
 *                             5.0.7 (and in table creation queries made with DB 
 *                             Forge). There is an incompatibility in PHP with 
 *                             mysql_real_escape_string() which can make your 
 *                             site vulnerable to SQL injection if you are using 
 *                             a multi-byte character set and are running versions 
 *                             lower than these. Sites using *Latin-1* or *UTF-8* 
 *                             database character set and collation are unaffected.
 *     @type string 'swap_pre' A default table prefix that should be swapped with 
 *                             the dbprefix.
 *     @type mixed  'encrypt'  Whether to use an encrypted connection. `'mysql'` (deprecated),
 *                             `'sqlsrv'` and `'pdo/sqlsrv'` drivers accept `true`|`false`.
 *                             `'mysqli'` and `'pdo/mysql'` drivers accept an array {
 *                                 @type string 'ssl_key'    Path to the private key file.
 *                                 @type string 'ssl_cert'   Path to the public key certificate file.
 *                                 @type string 'ssl_ca'     Path to the certificate authority file.
 *                                 @type string 'ssl_capath' Path to a directory containing trusted CA 
 *                                                           certificats in PEM format.
 *                                 @type string 'ssl_cipher` List of *allowed* ciphers to be used for 
 *                                                           the encryption, separated by colons `:`.
 *                                 @type bool 'ssl_verify'   Whether verify the server certificate or not 
 *                                                           ('mysqli' only). Accepts `true` or `false`.
 *                             }
 *     @type bool   'compress' Whether or not to use client compression (MySQL only).
 *                             Accepts `true` or `false`.
 *     @type bool   'stricton' Forces 'Strict Mode' connections, good for ensuring 
 *                             strict SQL while developing. Accepts `true` or `false`.
 *     @type string 'ssl_options'  Used to set various SSL options that can be used 
 *                                 when making SSL connections.
 *     @type array  'failover'     An array with 0 or more data for connections if 
 *                                 the main should fail.
 *     @type bool   'save_queries' Whether to "save" all executed queries. Accepts `true` or 
 *                                 `false`. **Note:** Disabling this will also effectively 
 *                                 disable both $this->db->last_query() and profiling of DB 
 *                                 queries. When you run a query, with this setting set to 
 *                                 `true` (default), CodeIgniter will store the SQL statement 
 *                                 for debugging purposes. However, this may cause high 
 *                                 memory usage, especially if you run a lot of SQL queries. 
 *                                 Disable this to avoid that problem.
 * }
 *
 * @see See the user guide for documentation on [Database Connections]
 *      (https://codeigniter.com/user_guide/database/configuration.html).
 *
 * @todo  Check if 'ssl_options' is a valid database connection setting.
 */
$db['default'] = array(
    'dsn'      => '',
    'hostname' => 'localhost',
    'username' => 'root',
    'password' => 'root',
    'database' => 'sprint',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => TRUE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

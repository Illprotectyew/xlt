<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Library
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

use Myth\Route;

/**
 * Admin Controller
 */
class AdminController extends \Myth\Controllers\ThemedController {

    use \Myth\Auth\AuthTrait;

	/**
	 * Initialise the Admin Controller
	 *
	 * @return void
	 */
    public function __construct()
    {
        parent::__construct();

        $this->restrict( Route::named('login') );
    }
}
<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Database\Migration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * Migration: Create Xceptions Table
 *
 * Created by: SprintPHP
 * Created on: 2016-04-21 05:40am
 *
 * @property $dbforge
 */
class Migration_create_xceptions_table extends CI_Migration {

    /**
     * Imports the migration
     *
     * @return void
     */
    public function up ()
    {
        $fields = [
        'id' => [
            'type' => 'int',
            'unsigned' => true,
            'auto_increment' => true,
            'constraint' => 9,
        ],
        'created_by' => [
            'type' => 'int',
            'constraint' => 9,
            'default' => 0,
        ],
        'supervisor' => [
            'type' => 'int',
            'constraint' => 9,
            'default' => 0,
        ],
        'start' => [
            'type' => 'datetime',
        ],
        'end' => [
            'type' => 'datetime',
        ],
        'authorized_by' => [
            'type' => 'int',
            'constraint' => 9,
            'default' => 0,
        ],
        'reason' => [
            'type' => 'varchar',
            'constraint' => 255,
            'null' => true,
            'default' => '',
        ],
        'approved' => [
            'type' => 'tinyint',
            'constraint' => 1,
            'default' => 0,
        ],
        'approved_by' => [
            'type' => 'int',
            'constraint' => 9,
            'default' => 0,
        ],
        'approved_on' => [
            'type' => 'datetime',
        ],
        'created_on' => [
            'type' => 'datetime',
        ],
        'modified_on' => [
            'type' => 'datetime',
        ],
        'deleted' => [
            'type' => 'tinyint',
            'constraint' => 1,
            'default' => 0,
        ],    ];

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('xceptions', true, config_item('migration_create_table_attr') );
    
    }

    /**
     * Removes the migration
     *
     * @return void
     */
    public function down ()
    {
        $this->dbforge->drop_table('xceptions');
    }
}
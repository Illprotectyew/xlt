<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Database\Migration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * Migration: Create Settings Table
 *
 * Created by: CATT
 * Created on: 2014-10-20 18:49:26 pm
 *
 * @property $dbforge
 */
class Migration_create_settings_table extends CI_Migration {

    /**
     * Imports the migration
     *
     * @return void
     */
    public function up ()
    {
        $fields = [
            'name'  => [
                'type'  => 'varchar',
                'constraint' => 255
            ],
            'value' => [
                'type' => 'varchar',
                'constraint' => 255,
                'null'  => true
            ],
            'group' => [
                'type' => 'varchar',
                'constraint' => 255
            ]
        ];
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key(['name', 'group'], true);
        $this->dbforge->create_table('settings', true, config_item('migration_create_table_attr'));
    }

    /**
     * Removes the migration
     *
     * @return void
     */
    public function down ()
    {
        $this->dbforge->drop_table('settings');
    }
}

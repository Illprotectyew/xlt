<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><table class="row">
    <tr>
        <td>
            <h1>I knew we could fix that for you!</h1>

            <p>Hey there!</p>

            <p>Someone using this email (<?= $email ?>) just reset the account password. If that was you, then disregard these instructions
                and all will be well.</p>

            <p>If you did not do this, then you should reset your password immediately by visiting the following link, and clicking the Forgot Your Password link:</p>

            <p>
                <a href="<?= $site_link ?>">
                    <?= $link ?>
                </a>
            </p>

            <p>If the link does not work, please visit the following page:</p>

            <p><?= $link ?></p>

            <p>Thanks!<br/><?= $site_name ?></p>
        </td>
    </tr>
</table>

<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?>Hey there!

Someone using this email (<?= $email ?>) just signed up for an account at <?= $site_name ?>. If that was not you, then disregard these instructions
and all will be well.

If that was you - then click the link below to activate your account:

<?= $link ."?e={$email}&code={$token}" ?>

If the link does not work, please visit the following page: <?= $link ?> and enter the following token when asked:

<?= $token ?>

Thanks!
<?= $site_name ?>
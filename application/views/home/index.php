<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><div id="container">

    <h1>Welcome to Code All The Things!</h1>

    <br/>

    <?= $uikit->row([], function() use($uikit) {

        echo $uikit->column(['sizes' => ['l'=>6]], function() use($uikit) { ?>
            <h3>What Is CATT?</h3>

            <p>CATT is a souped-up version of <a href="http://codeigniter.com">CodeIgniter <?= CI_VERSION ?></a>. And soon to be the heart and soul
                of <a href="" target="_blank">Bonfire Next</a>. </p>

            <p>If you would like to edit this page you'll find it located at:</p>

            <code>application/views/home/index.php</code>

            <p>The corresponding controller for this page is found at:</p>

            <code>application/controllers/Home.php</code>

        <?php });





        echo $uikit->column(['sizes' => ['l'=>6]], function() use($uikit) { ?>
            <h3>Get To Know CATT</h3>

            <p>The following resources will help you as you explore the power and flexibility that CATT provides. Feel free to dig into source code of the controllers and views
                to really discover how things are working. You never know what buried treasure you'll find!</p>

            <ul>
                <li><a href="http://ci3docs.cibonfire.com" target="_blank">CodeIgniter 3 User Guide</a></li>
                <li><a href="<?= site_url('docs') ?>">CATT Documentation</a></li>
                <li><a href="https://github.com/ci-bonfire/sprint-demos">CATT Demos Repo</a></li>
            </ul>

        <?php });

    }); ?>

</div>

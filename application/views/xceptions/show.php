<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><h2>View Time Exception</h2>

<table class="table">
    <tbody>
        <tr>
            <td>Start</td>
            <td><?= $item->start ?></td>
        </tr>
        <tr>
            <td>End</td>
            <td><?= $item->end ?></td>
        </tr>
        <tr>
            <td>Authorized By</td>
            <td><?= $item->authorized_by ?></td>
        </tr>
        <tr>
            <td>Reason</td>
            <td><?= $item->reason ?></td>
        </tr>
        <tr>
            <td>Approved</td>
            <td><?= $item->approved ?></td>
        </tr>
        <tr>
            <td>Approved By</td>
            <td><?= $item->approved_by ?></td>
        </tr>
        <tr>
            <td>Approved On</td>
            <td><?= $item->approved_on ?></td>
        </tr>
        <tr>
            <td>Created On</td>
            <td><?= $item->created_on ?></td>
        </tr>
        <tr>
            <td>Modified On</td>
            <td><?= $item->modified_on ?></td>
        </tr>
    </tbody>
</table>

<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><h2>Time Exceptions</h2>

<a href='/admin/xceptions/create' class='btn btn-primary ' role='button'>Add New</a>

<hr/>

<?php if ( ! empty($rows) && is_array($rows) && count($rows) ) : ?>

    <?php $headers = array_keys($rows[0]); ?>

    <table class="table">
        <thead>
            <tr>
                <?php foreach ($headers as $name) : ?>
                    <th><?= $name ?></th>
                <?php endforeach; ?>
                    <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($rows as $row) : ?>
                <tr>
                    <?php foreach ($headers as $key) : ?>
                        <td><?= $row[$key] ?></td>
                    <?php endforeach; ?>
                        <td>
                            <a href="/admin/xceptions/update/<?= $row['id'] ?>">Edit</a> |
                            <a href="/admin/xceptions/delete/<?= $row['id'] ?>" onclick="return confirm('Delete this exception?');">Delete</a>
                        </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php else : ?>

    <div class='alert alert-warning '>
    	Unable to find any exceptions.
    	<a href='#' class='close'>&times;</a>
    </div>

<?php endif; ?>

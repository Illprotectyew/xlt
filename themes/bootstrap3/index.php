<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?><!-- Header -->
<?= $themer->display('bootstrap:blocks/header') ?>

<!-- Content -->
<div class="content <?= $containerClass ?>">
    <?= $notice ?>
    <?= $view_content ?>
</div><!-- /.content -->

<!-- Footer -->
<?= $themer->display('bootstrap:blocks/footer') ?>

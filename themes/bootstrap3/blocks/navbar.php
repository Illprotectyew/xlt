<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?><!-- Navbar -->
<header class="navbar navbar-inverse <?= $navbar_style ?>" id="navbar" role="banner">
    <div class="<?= $containerClass ?>">
        <div class="navbar-header">

            <!-- Toggle -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button><!-- /.navbar-toggle -->
            
            <!-- Brand -->
            <a class="navbar-brand" href="<?= site_url() ?>">
                <img height="40" src="<?= site_url('themes/bootstrap3/images/catt.png') ?>">
                <?= config_item('site.name') ?>
            </a><!-- /.navbar-brand -->

        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">

            <!-- Main Menu -->
            <ul id="main-menu" class="nav navbar-nav navbar-left">
                <li class="active"><a href="#">Link</a></li>
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" >Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
            </ul><!-- /#main-menu -->

            <!-- User Menu -->
            <ul id="user-menu" class="nav navbar-nav navbar-right">
                <li>
                    <?php if (! empty($_SESSION['logged_in']) ) : ?>
                        <a href="<?= site_url( \Myth\Route::named('logout') ) ?>">Logout</a>
                    <?php else : ?>
                        <a href="<?= site_url( \Myth\Route::named('login') ) ?>">Login</a>
                    <?php endif; ?>
                </li>
            </ul><!-- /#user-menu -->

        </div>
    </div>
</header><!-- /.navbar -->
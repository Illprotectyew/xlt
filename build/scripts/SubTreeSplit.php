<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Build
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

use Myth\CLI;

/**
 * Class SubTreeSplit
 *
 * A standalone build script for publishing the readonly subtree splits.
 *
 * REQUIRES:
 *      https://github.com/dflydev/git-subsplit
 */
class SubTreeSplit extends BaseBuilder {

	/**
	 * Runs the different tasks
	 */
    public function run()
    {
        // You Must Construct Additional Pylons
    }
}
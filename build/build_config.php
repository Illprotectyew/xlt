<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 *  Builds
 * 
 *  Our build system supports multiple build scripts that can be used.
 *  This array holds the alias of the build script as the key, and
 *  the name of the build class file itself that will be run
 *  to perform that build.
 */
$config['builds'] = [
    'release'           => 'SprintRelease',
    'postCreateProject' => 'InitialCleanup',
    'publishSubTrees'   => 'SubTreeSplit'
];

/**
 *  Destination Folders
 *
 *  A list of folders that the results of the build scripts will use
 *  as their root folder. The key should be the alias of the build,
 *  as defined in $config['builds']. The value is the relative path
 *  to the 'build' folder. If this folder does not exist, it will be
 *  created.
 */
$config['destinations'] = [
    'release'           => '../../SprintBuilds/',
    'postCreateProject' => ''
];

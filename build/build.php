<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Build
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * BUILD TOOL
 *
 * This is a simple tool to create the releases for Sprint. The primary tasks are:
 * - Create a zip file of final state of code ready for upload
 * - Remove our tests folder
 * - Remove any logs and cache items from the app that it might have
 * - remove this build folder
 */

use Myth\CLI;

$start_time = microtime(true);

// Ensure errors are on
error_reporting(-1);
ini_set('display_errors', 1);

// Load our autoloader
require __DIR__ .'/../vendor/autoload.php';

// Load configuration
require 'build_config.php';

// Folder definitions
define('BUILDBASE', __DIR__ .'/');

// Don't stop script exectuion on 404...
function show_404($page = '', $log_error = TRUE) {}

// Make sure we have access to CI() object.
ob_start();
	require( BUILDBASE .'../index.php' );
ob_end_clean();


/**
 * Determine the build script to run
 */

$release = CLI::segment(1);

if (empty($release))
{
	$release = CLI::prompt("Which script", array_keys($config['builds']) );
}

if (! array_key_exists($release, $config['builds']))
{
	CLI::error('Invalid build specified: '. $release);
	exit(1);
}


/**
 * Instantiate the class and run it
 */

$class_name = $config['builds'][$release];


if (! file_exists(BUILDBASE ."scripts/{$class_name}.php"))
{
	CLI::error('Unable to find build script: '. $class_name .'.php');
	exit(1);
}

require BUILDBASE ."lib/BaseBuilder.php";
require BUILDBASE ."scripts/{$class_name}.php";

$builder = new $class_name( $config['destinations'][$release], get_instance() );

if (! is_object($builder))
{
	CLI::error('Unable to make new class: '. $class_name);
	exit(1);
}

// run it!
CLI::write("Running builder `{$release}` ({$class_name})...", 'yellow');

$builder->run();


/**
 * Show closing comments
 */

$end_time = microtime(true);
$elapsed_time = number_format($end_time - $start_time, 4);

CLI::write('Done in '. $elapsed_time .' seconds', 'green');
# Sprint Minimal Setup
The minimum setup required for Sprint. The `base_url` must end with `.dev` or 
the development configuration will not be loaded.


### Install with Composer
See [Reference](https://github.com/ci-bonfire/Sprint/blob/develop/myth/_docs_src/installation.md) installation.

Automatic install:
```bash
php composer.phar create-project sprintphp/sprintphp xlt dev-develop
```


Manual install:
```bash
composer install
composer dump-autoload
php build/build.php postCreateProject
composer dump-autoload -o
```


### Configure Sprint
Create, Modify or Delete Files & Directories:

- Create `/application/config/development/config.php`
    - Add `$config['base_url'] = 'http://xlt.dev';`


### Migrate the Database

```bash
php sprint database migrate
```


### Configure Application
Create, Modify or Delete Files & Directories:

- Modify `/application/config/application.php`
    - Edit `$config['site.name']        = 'Exceptionally Timed';`
    - Edit `$config['site.auth_email']  = 'jlareaux@godaddy.com'`
    - Edit `$config['use_php_error'] = TRUE;`

- Modify `/application/config/auth.php`
    - Edit `$config['auth.default_role_id'] = 1;`


### Seed the Database

```bash
php sprint database seed FlatAuthorizationSeeder
```

### Login
Signup and then login to Sprint.